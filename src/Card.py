def mapping_value(inp: str) -> int:
    if inp == 'J':
        value = 11
    elif inp == 'Q':
        value = 12
    elif inp == 'K':
        value = 13
    elif inp == 'A':
        value = 14
    else:
        value = int(inp)
    return value


def convert_to_card(cards: list) -> list:
    result_cards: list = []
    for c in cards:
        result_cards.append([mapping_value(c[0:len(c)-1]), c[-1]])
    return result_cards


def is_flush(cards: list) -> bool:
    cards = convert_to_card(cards)
    denoted: list = []
    for c in cards:
        denoted.append(c[1])
    return len(set(denoted)) == 1


def is_straight(cards: list) -> bool:
    cards = convert_to_card(cards)
    values: list = []
    for c in cards:
        values.append(c[0])
    values.sort(reverse=True)
    value_x: int = values[0] + 1
    for v in values:
        if value_x - v == 1:
            value_x = v
        else:
            return False
    return True


def is_royal(cards: list) -> bool:
    cards = convert_to_card(cards)
    cards.sort()
    return cards[0][0] == 10 and cards[1][0] == 11 and cards[2][0] == 12 and cards[3][0] == 13 and cards[4][0] == 14


def is_royal_flush(cards: list) -> bool:
    return is_royal(cards) and is_flush(cards)


def is_straight_flush(cards: list) -> bool:
    return is_straight(cards) and is_flush(cards) and not is_royal(cards)


def group_cards(cards: list) -> list:
    cards = convert_to_card(cards)
    values: list = []
    group_card: list = []
    for c in cards:
        values.append(c[0])
    set_card: set = set(values)
    for s in set_card:
        group_card.append([s, values.count(s)])
    return group_card


def is_four_of_kind(cards: list) -> list:
    group_card: list = group_cards(cards)
    for g in group_card:
        if g[1] == 4:
            return [True, g[0]]
    return [False]


def is_three_of_kind(cards: list) -> list:
    group_card: list = group_cards(cards)
    for g in group_card:
        if g[1] == 3:
            return [True, g[0]]
    return [False]


def get_pair(cards: list) -> list:
    group_card: list = group_cards(cards)
    results: list = []
    for g in group_card:
        if g[1] == 2:
            results.append(g[0])
    return results


def is_two_pair(cards: list) -> list:
    pair_card: list = get_pair(cards)
    pair_card.sort(reverse=True)
    if len(pair_card) == 2:
        return [True, pair_card[0]]
    return [False]


def is_pair(cards: list) -> list:
    pair_card: list = get_pair(cards)
    if len(pair_card) == 1:
        pair_card.insert(0, True)
        return pair_card
    return [False]


def is_full_house(cards: list) -> bool:
    return is_three_of_kind(cards)[0] and is_pair(cards)[0]


def get_high_card(cards: list) -> int:
    group_card: list = group_cards(cards)
    one_card: list = []
    for g in group_card:
        if g[1] == 1:
            one_card.append(g[0])
    one_card.sort(reverse=True)
    return int(one_card[0])


def get_rank(cards: list) -> list:
    result: list = []
    _is_royal_flush: bool = is_royal_flush(cards)
    _is_straight_flush: bool = is_straight_flush(cards)
    _is_four_of_kind: list = is_four_of_kind(cards)
    _is_full_house: bool = is_full_house(cards)
    _is_flush: bool = is_flush(cards)
    _is_straight: bool = is_straight(cards)
    _is_three_of_kind: list = is_three_of_kind(cards)
    _is_two_pair: list = is_two_pair(cards)
    _is_pair: list = is_pair(cards)

    if _is_royal_flush:
        result.append('ROYAL_FLUSH')
    elif _is_straight_flush:
        result.append('STRAIGHT_FLUSH')
    elif _is_four_of_kind[0]:
        result.append('FOUR_OF_KIND')
        result.append(_is_four_of_kind[1])
    elif _is_full_house:
        result.append('FULL_HOUSE')
    elif _is_flush:
        result.append('FLUSH')
    elif _is_straight:
        result.append('STRAIGHT')
    elif _is_three_of_kind[0]:
        result.append('THREE_OF_KIND')
        result.append(_is_three_of_kind[1])
    elif _is_two_pair[0]:
        result.append('TWO_PAIR')
        result.append(_is_two_pair[1])
    elif _is_pair[0]:
        result.append('PAIR')
        result.append(_is_pair[1])
    else:
        result.append('HIGH_CARD')
        result.append(get_high_card(cards))

    return result
