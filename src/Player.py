from src.Card import get_rank
from src.Card import get_high_card


class Player:
    def __init__(self, name: str, cards: list):
        self.name: str = name
        self.cards: list = cards
        self.high_score: int = 0

    def get_rank(self) -> list:
        return get_rank(self.cards)

    def get_high_score(self) -> int:
        return get_high_card(self.cards)
