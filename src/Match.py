from src.Player import Player


def rank_score(rank: str):
    score: int = 0
    if rank == 'ROYAL_FLUSH':
        score = 10
    elif rank == 'STRAIGHT_FLUSH':
        score = 9
    elif rank == 'FOUR_OF_KIND':
        score = 8
    elif rank == 'FULL_HOUSE':
        score = 7
    elif rank == 'FLUSH':
        score = 6
    elif rank == 'STRAIGHT':
        score = 5
    elif rank == 'THREE_OF_KIND':
        score = 4
    elif rank == 'TWO_PAIR':
        score = 3
    elif rank == 'PAIR':
        score = 2
    elif rank == 'HIGH_CARD':
        score = 1
    return score


def encode_score(score: int) -> str:
    if score == 11:
        result = 'Jack'
    elif score == 12:
        result = 'Queen'
    elif score == 13:
        result = 'King'
    elif score == 14:
        result = 'Ace'
    else:
        result = str(score)
    return result


class Match:
    def __init__(self, player_1: Player, player_2: Player):
        self.player_1 = player_1
        self.player_2 = player_2

    def duel(self) -> str:
        rank_1: list = self.player_1.get_rank()
        rank_score_1: int = rank_score(rank_1[0])
        rank_2: list = self.player_2.get_rank()
        rank_score_2: int = rank_score(rank_2[0])
        if rank_score_1 == rank_score_2:
            if rank_score_1 in (8, 4, 3, 2, 1):
                if rank_1[1] > rank_2[1]:
                    result = f'{self.player_1.name}: {rank_1[0]} [{encode_score(rank_1[1])}] win'
                elif rank_1[1] < rank_2[1]:
                    result = f'{self.player_2.name}: {rank_2[0]} [{encode_score(rank_2[1])}] win'
                else:
                    high_score_1: int = self.player_1.get_high_score()
                    high_score_2: int = self.player_2.get_high_score()
                    if high_score_1 > high_score_2:
                        result = f'{self.player_1.name}: {rank_1[0]} [{encode_score(rank_1[1])}] ' \
                                 f'High score [{encode_score(high_score_1)}] win'
                    elif high_score_1 < high_score_2:
                        result = f'{self.player_2.name}: {rank_2[0]} [{encode_score(rank_2[1])}] ' \
                                 f'High score [{encode_score(high_score_2)}] win'
                    else:
                        result = f'{self.player_1.name}: {rank_1[0]} [{encode_score(rank_1[1])}] ' \
                                 f'High score [{encode_score(high_score_1)}] win, ' \
                                 f'{self.player_2.name}: {rank_2[0]} [{encode_score(rank_2[1])}] ' \
                                 f'High score [{encode_score(high_score_2)}] win'
            else:
                result = f'{self.player_1.name}: {rank_1[0]} win, {self.player_2.name}: {rank_1[0]} win'
        elif rank_score_1 > rank_score_2:
            result = f'{self.player_1.name}: {rank_1[0]} win'
        else:
            result = f'{self.player_2.name}: {rank_2[0]} win'
        return result
