import unittest


from src.Match import Match
from src.Player import Player


class TestInit(unittest.TestCase):
    def test_initial_match(self) -> None:
        player_1: Player = Player("White", ['10S', 'JS', 'QS', 'KS', 'AS'])
        player_2: Player = Player("Black", ['10C', 'JC', 'QC', 'KC', 'AC'])
        match: Match = Match(player_1, player_2)
        self.assertEqual(player_1, match.player_1)
        self.assertEqual(player_2, match.player_2)


class TestDuel(unittest.TestCase):
    def test_duel_royal_flush_vs_royal_flush(self):
        player_1: Player = Player("White", ['10S', 'JS', 'QS', 'KS', 'AS'])
        player_2: Player = Player("Black", ['10C', 'JC', 'QC', 'KC', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: ROYAL_FLUSH win, Black: ROYAL_FLUSH win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_royal_flush_vs_straight_flush(self):
        player_1: Player = Player("White", ['10S', 'JS', 'QS', 'KS', 'AS'])
        player_2: Player = Player("Black", ['10C', 'JC', 'QC', 'KC', '9C'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: ROYAL_FLUSH win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_straight_flush_vs_royal_flush(self):
        player_1: Player = Player("White", ['10S', 'JS', 'QS', 'KS', '9S'])
        player_2: Player = Player("Black", ['10C', 'JC', 'QC', 'KC', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "Black: ROYAL_FLUSH win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_10_vs_four_of_kind_9(self):
        player_1: Player = Player("White", ['10C', '10D', '10H', '10S', 'AS'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', '9S', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: FOUR_OF_KIND [10] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_9_vs_four_of_kind_10(self):
        player_1: Player = Player("White", ['9C', '9D', '9H', '9S', 'AC'])
        player_2: Player = Player("Black", ['10C', '10D', '10H', '10S', 'AS'])
        match: Match = Match(player_1, player_2)
        expect: str = "Black: FOUR_OF_KIND [10] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_J_vs_four_of_kind_9(self):
        player_1: Player = Player("White", ['JC', 'JD', 'JH', 'JS', 'AS'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', '9S', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: FOUR_OF_KIND [Jack] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_Q_vs_four_of_kind_9(self):
        player_1: Player = Player("White", ['QC', 'QD', 'QH', 'QS', 'AS'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', '9S', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: FOUR_OF_KIND [Queen] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_K_vs_four_of_kind_9(self):
        player_1: Player = Player("White", ['KC', 'KD', 'KH', 'KS', 'AS'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', '9S', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: FOUR_OF_KIND [King] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_four_of_kind_A_vs_four_of_kind_9(self):
        player_1: Player = Player("White", ['AC', 'AD', 'AH', 'AS', '2S'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', '9S', '2C'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: FOUR_OF_KIND [Ace] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_three_of_kind_10_vs_three_of_kind_9(self):
        player_1: Player = Player("White", ['10C', '10D', '10H', 'KH', 'AH'])
        player_2: Player = Player("Black", ['9C', '9D', '9H', 'KS', 'AS'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: THREE_OF_KIND [10] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_two_pair_10_11_vs_two_pair_8_9(self):
        player_1: Player = Player("White", ['10C', '10D', 'JC', 'JD', 'AC'])
        player_2: Player = Player("Black", ['8C', '8D', '9C', '9D', 'AD'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: TWO_PAIR [Jack] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_two_pair_10_11_A_vs_two_pair_8_11_K(self):
        player_1: Player = Player("White", ['10C', '10D', 'JC', 'JD', 'AC'])
        player_2: Player = Player("Black", ['8C', '8D', 'JH', 'JS', 'KC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: TWO_PAIR [Jack] High score [Ace] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_two_pair_10_11_K_vs_two_pair_8_11_A(self):
        player_1: Player = Player("White", ['10C', '10D', 'JC', 'JD', 'KC'])
        player_2: Player = Player("Black", ['8C', '8D', 'JH', 'JS', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "Black: TWO_PAIR [Jack] High score [Ace] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_two_pair_10_11_A_vs_two_pair_8_11_A(self):
        player_1: Player = Player("White", ['10C', '10D', 'JC', 'JD', 'AD'])
        player_2: Player = Player("Black", ['8C', '8D', 'JH', 'JS', 'AC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: TWO_PAIR [Jack] High score [Ace] win, Black: TWO_PAIR [Jack] High score [Ace] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_pair_10_A_vs_two_pair_8_A(self):
        player_1: Player = Player("White", ['10C', '10D', 'JC', 'QD', 'AC'])
        player_2: Player = Player("Black", ['8C', '8D', 'JH', 'QH', 'KC'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: PAIR [10] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_pair_8_A_vs_pair_10_A(self):
        player_1: Player = Player("White", ['8C', '8D', 'JC', 'QD', 'AC'])
        player_2: Player = Player("Black", ['10C', '10D', 'JH', 'QH', 'KC'])
        match: Match = Match(player_1, player_2)
        expect: str = "Black: PAIR [10] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_pair_10_A_vs_pair_10_A(self):
        player_1: Player = Player("White", ['10H', '10D', 'JC', 'QC', 'AC'])
        player_2: Player = Player("Black", ['10C', '10S', 'JH', 'QH', 'KH'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: PAIR [10] High score [Ace] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)

    def test_duel_high_card_10_A_vs_two_high_card_8_A(self):
        player_1: Player = Player("White", ['3D', '10C', 'JC', 'QC', 'KC'])
        player_2: Player = Player("Black", ['2C', '9D', '10D', 'JD', 'QD'])
        match: Match = Match(player_1, player_2)
        expect: str = "White: HIGH_CARD [King] win"
        actual: str = match.duel()
        self.assertEqual(expect, actual)
