import unittest

from src.Card import *


class TestMappingValue(unittest.TestCase):
    def test_mapping_value_1(self):
        inp: str = "1"
        expect: int = 1
        actual: int = mapping_value(inp)
        self.assertEqual(expect, actual)

    def test_mapping_value_J(self):
        inp: str = "J"
        expect: int = 11
        actual: int = mapping_value(inp)
        self.assertEqual(expect, actual)

    def test_mapping_value_Q(self):
        inp: str = "Q"
        expect: int = 12
        actual: int = mapping_value(inp)
        self.assertEqual(expect, actual)

    def test_mapping_value_K(self):
        inp: str = "K"
        expect: int = 13
        actual: int = mapping_value(inp)
        self.assertEqual(expect, actual)

    def test_mapping_value_A(self):
        inp: str = "A"
        expect: int = 14
        actual: int = mapping_value(inp)
        self.assertEqual(expect, actual)


class TestConvertToCard(unittest.TestCase):
    def test_convert_to_card(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        expect: list = [[10, 'S'], [11, 'S'], [12, 'S'], [13, 'S'], [14, 'S']]
        actual: list = convert_to_card(inputs)
        self.assertEqual(expect, actual)


class TestIsFlush(unittest.TestCase):
    def test_is_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_flush(inputs)
        self.assertTrue(actual)

    def test_is_not_flush(self):
        inputs: list = ['10S', 'JS', 'QC', 'KS', 'AS']
        actual: bool = is_flush(inputs)
        self.assertFalse(actual)


class TestIsStraight(unittest.TestCase):
    def test_is_straight(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_straight(inputs)
        self.assertTrue(actual)

    def test_is_not_straight(self):
        inputs: list = ['9S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_straight(inputs)
        self.assertFalse(actual)


class TestIsRoyal(unittest.TestCase):
    def test_is_royal(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_royal(inputs)
        self.assertTrue(actual)

    def test_is_royal_not_order(self):
        inputs: list = ['JS', 'QS', '10S', 'KS', 'AS']
        actual: bool = is_royal(inputs)
        self.assertTrue(actual)

    def test_is_royal_not_flush(self):
        inputs: list = ['10S', 'JS', 'QC', 'KS', 'AS']
        actual: bool = is_royal(inputs)
        self.assertTrue(actual)

    def test_is_not_royal(self):
        inputs: list = ['9S', '10S', 'JS', 'QS', 'KS']
        actual: bool = is_royal(inputs)
        self.assertFalse(actual)


class TestIsRoyalFlush(unittest.TestCase):
    def test_is_royal_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_royal_flush(inputs)
        self.assertTrue(actual)

    def test_is_not_royal(self):
        inputs: list = ['9S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_royal_flush(inputs)
        self.assertFalse(actual)

    def test_is_not_flush(self):
        inputs: list = ['10S', 'JS', 'QC', 'KS', 'AS']
        actual: bool = is_royal_flush(inputs)
        self.assertFalse(actual)


class TestIsStraightFlush(unittest.TestCase):
    def test_is_straight_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', '9S']
        actual: bool = is_straight_flush(inputs)
        self.assertTrue(actual)

    def test_is_not_straight_flush_is_royal(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: bool = is_straight_flush(inputs)
        self.assertFalse(actual)

    def test_is_not_straight_flush_is_not_straight(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', '8S']
        actual: bool = is_straight_flush(inputs)
        self.assertFalse(actual)

    def test_is_not_straight_flush_is_not_flush(self):
        inputs: list = ['10S', 'JS', 'QC', 'KS', '9S']
        actual: bool = is_straight_flush(inputs)
        self.assertFalse(actual)


class TestGroupCards(unittest.TestCase):
    def test_group_cards_four_kind(self):
        inputs: list = ['10C', '10D', '10H', '10S', 'AS']
        actual: list = group_cards(inputs)
        expect: list = [[10, 4], [14, 1]]
        self.assertEqual(expect, actual)

    def test_group_cards_full_house(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = group_cards(inputs)
        expect: list = [[10, 3], [14, 2]]
        self.assertEqual(expect, actual)

    def test_group_cards_three_kind(self):
        inputs: list = ['10C', '10D', '10H', 'KH', 'AS']
        actual: list = group_cards(inputs)
        expect: list = [[10, 3], [13, 1], [14, 1]]
        self.assertEqual(expect, actual)

    def test_group_cards_two_pair(self):
        inputs: list = ['10C', '10D', 'KC', 'KD', 'AS']
        actual: list = group_cards(inputs)
        expect: list = [[10, 2], [13, 2], [14, 1]]
        self.assertEqual(expect, actual)

    def test_group_cards_pair(self):
        inputs: list = ['10C', '10D', 'QC', 'KD', 'AS']
        actual: list = group_cards(inputs)
        expect: list = [[10, 2], [12, 1], [13, 1], [14, 1]]
        self.assertEqual(expect, actual)


class TestFourOfKind(unittest.TestCase):
    def test_is_four_of_kind(self):
        inputs: list = ['10C', '10D', '10H', '10S', 'AS']
        actual: list = is_four_of_kind(inputs)
        expect: list = [True, 10]
        self.assertEqual(expect, actual)

    def test_is_not_four_of_kind(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = is_four_of_kind(inputs)
        expect: list = [False]
        self.assertEqual(expect, actual)


class TestThreeOfKind(unittest.TestCase):
    def test_is_three_of_kind(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = is_three_of_kind(inputs)
        expect: list = [True, 10]
        self.assertEqual(expect, actual)

    def test_is_not_three_of_kind_four_kind(self):
        inputs: list = ['10C', '10D', '10H', '10S', 'AS']
        actual: list = is_three_of_kind(inputs)
        expect: list = [False]
        self.assertEqual(expect, actual)

    def test_is_not_three_of_kind_pair(self):
        inputs: list = ['10C', '10D', '9H', 'AH', 'AS']
        actual: list = is_three_of_kind(inputs)
        expect: list = [False]
        self.assertEqual(expect, actual)


class TestGetPair(unittest.TestCase):
    def test_get_pair_one_pair(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = get_pair(inputs)
        expect: list = [14]
        self.assertEqual(expect, actual)

    def test_get_pair_two_pair(self):
        inputs: list = ['10C', '10D', 'JH', 'AH', 'AS']
        actual: list = get_pair(inputs)
        expect: list = [10, 14]
        self.assertEqual(expect, actual)

    def test_get_pair_fail(self):
        inputs: list = ['10C', 'JD', 'QH', 'KH', 'AS']
        actual: list = get_pair(inputs)
        expect: list = []
        self.assertEqual(expect, actual)


class TestIsTwoPair(unittest.TestCase):
    def test_is_two_pair(self):
        inputs: list = ['10C', '10D', 'JH', 'AH', 'AS']
        actual: list = is_two_pair(inputs)
        expect: list = [True, 14]
        self.assertEqual(expect, actual)

    def test_is_not_two_pair_one_pair(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = is_two_pair(inputs)
        expect: list = [False]
        self.assertEqual(expect, actual)


class TestIsPair(unittest.TestCase):
    def test_is_pair_full_house(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = is_pair(inputs)
        expect: list = [True, 14]
        self.assertEqual(expect, actual)

    def test_is_pair(self):
        inputs: list = ['10C', 'JD', 'QH', 'AH', 'AS']
        actual: list = is_pair(inputs)
        expect: list = [True, 14]
        self.assertEqual(expect, actual)

    def test_is_not_pair_two_pair(self):
        inputs: list = ['10C', '10D', 'JH', 'AH', 'AS']
        actual: list = is_pair(inputs)
        expect: list = [False]
        self.assertEqual(expect, actual)


class TestFullHouse(unittest.TestCase):
    def test_is_full_house(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: bool = is_full_house(inputs)
        self.assertTrue(actual)


class TestHighCard(unittest.TestCase):
    def test_get_high_card(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: int = get_high_card(inputs)
        expect: int = 14
        self.assertEqual(actual, expect)

    def test_get_high_card_two_pair(self):
        inputs: list = ['10C', '10D', 'JH', 'AH', 'AS']
        actual: int = get_high_card(inputs)
        expect: int = 11
        self.assertEqual(actual, expect)

    def test_get_high_card_pair(self):
        inputs: list = ['10C', '10D', 'QC', 'KD', 'AS']
        actual: int = get_high_card(inputs)
        expect: int = 14
        self.assertEqual(actual, expect)


class TestGetRank(unittest.TestCase):
    def test_get_rank_royal_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['ROYAL_FLUSH']
        self.assertEqual(actual, expect)

    def test_get_rank_straight_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', '9S']
        actual: list = get_rank(inputs)
        expect: list = ['STRAIGHT_FLUSH']
        self.assertEqual(actual, expect)

    def test_get_rank_four_of_kind(self):
        inputs: list = ['10C', '10D', '10H', '10S', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['FOUR_OF_KIND', 10]
        self.assertEqual(actual, expect)

    def test_get_rank_full_house(self):
        inputs: list = ['10C', '10D', '10H', 'AH', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['FULL_HOUSE']
        self.assertEqual(actual, expect)

    def test_get_rank_flush(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', '1S']
        actual: list = get_rank(inputs)
        expect: list = ['FLUSH']
        self.assertEqual(actual, expect)

    def test_get_rank_straight(self):
        inputs: list = ['10S', 'JS', 'QS', 'KS', 'AC']
        actual: list = get_rank(inputs)
        expect: list = ['STRAIGHT']
        self.assertEqual(actual, expect)

    def test_get_rank_three_of_kind(self):
        inputs: list = ['10C', '10D', '10H', 'KH', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['THREE_OF_KIND', 10]
        self.assertEqual(actual, expect)

    def test_get_rank_two_pair(self):
        inputs: list = ['10C', '10D', 'JH', 'AH', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['TWO_PAIR', 14]
        self.assertEqual(actual, expect)

    def test_get_rank_pair(self):
        inputs: list = ['10C', '10D', 'QC', 'KD', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['PAIR', 10]
        self.assertEqual(actual, expect)

    def test_get_rank_high_card(self):
        inputs: list = ['2S', 'JS', 'QC', 'KS', 'AS']
        actual: list = get_rank(inputs)
        expect: list = ['HIGH_CARD', 14]
        self.assertEqual(actual, expect)
