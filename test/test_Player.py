import unittest


from src.Player import Player


class TestPlayer(unittest.TestCase):
    def setUp(self) -> None:
        self.player = Player("White", ['10S', 'JS', 'QS', 'KS', 'AS'])


class TestInit(TestPlayer):
    def test_initial_player(self):
        self.assertEqual(self.player.name, "White")
        self.assertEqual(self.player.cards, ['10S', 'JS', 'QS', 'KS', 'AS'])
        self.assertEqual(self.player.high_score, 0)


class TestGetRank(TestPlayer):
    def test_get_rank(self):
        actual: list = self.player.get_rank()
        expect: list = ['ROYAL_FLUSH']
        self.assertEqual(actual, expect)


class TestGetHighScore(TestPlayer):
    def test_get_high_score(self):
        actual: int = self.player.get_high_score()
        expect: int = 14
        self.assertEqual(actual, expect)
